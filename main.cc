/*
 *  // - comment for better understanding
 *   /// - TODO - should be removed and code reworked (comments updated)
 *    have fun ;)
 *    */

#include <iostream>
#include <string>
#include <vector>

using namespace std;

/// make it singleton, load status, rates, and msgs from file on init/use, change it to work without enum
/// split this to class based files header/code and main for combat logic

// for mapping health enum to string, its susceptible to break program and errors if editing/adding leisurely
const char* healthStr[] = { "is unharmed", "have few scratches", "is visible injured", "have sewer injuries", "is near death ", "is dead" };

// class encapsulating all possible statuses of character, actualy only health but can contain attributes and methods for others too, like paralyse, boosts, debufs
class Status {
	public:
		enum Health { NOHARM, FINE, INJURED, BLOODY, DYING, DEAD, MAX_HEALTH };
	private:
	public:
		// static F. for  health status logic
		static Health getHealth(double rate) {
			if (rate > 0.99) return NOHARM;
			if (rate > 0.8) return FINE;
			else if (rate > 0.4) return INJURED;
			else if (rate > 0.15 ) return BLOODY;
			else if (rate > 0) return DYING;
			else return DEAD;
		}
		// static F. for mapping status to string. should be overloaded for different status types
		static const char* write(Health status) { return healthStr[status]; };
};

/// make it loadable from file
// structure describing attacks
struct Attack {
	string name;
	double mp;
	double dmg;
	Attack(string n, double mp, double dmg) : name(n), mp(mp), dmg(dmg) {};
};

/// make it loadable from file, use use attack name/id to be able to use attacks by reference to other object
/// attack method should take list of attacks as param and choose from that list instead
// dueling character for battle
class Character {
	double maxHP;
	double HP;
	double maxMP;
	double MP;
	string name;
	Status::Health health = Status::getHealth(1.0);
	vector<Attack> attacks;
	public:
	/// might be set during init as well but could stay as separete method if more logic then assignemnt should be done
	// assign known/usable attacks for character
	void setAttacks(const vector<Attack> &att) { attacks = att; cout << name <<" knows " << attacks.size() <<" attacks" <<endl; };
	// deal damage to character
	void dealDmg(double dmg) { HP -= dmg; health = Status::getHealth(HP / maxHP); };
	/// consider non skill attacks
	// make an attack, do zero dmg if not able to use skill
	double attack(size_t index) {
		if (index >= attacks.size()) return 0.0;

		Attack &atk = attacks[index];
		if (MP < atk.mp) return 0.0;

		MP -= atk.mp;
		cout << name << " used " << atk.name <<endl;
		///consider dmg based on health status, injured characters dont hit as hard but might have some skill to hit the same or even harder!
		return atk.dmg;
	};
	/// make restore MP character dependent
	// restore portion of charaters MP
	void restoreMP() { double restore = maxMP / 3; restore = min(MP + restore, maxMP); cout << name <<  " is restoring " << restore - MP <<" MP" <<endl; MP = restore; };

	// getter methods
	const string& getName() const { return name; };
	bool isAlive() const { return health != Status::DEAD; };

	// write to console methods
	/// consider custom output stream
	void writeStatus() const { cout << name <<" "<<Status::write(health) <<endl; };
	void writeStatusFull() const { cout << name << ", " << HP <<"/" <<maxHP <<" hp, " <<MP <<"/" <<maxMP <<" mp, " << attacks.size() <<" different attacks." <<endl; };

	// constructors
	/// should deklared first in class as good programing practise but it have to be here as its a definition using other methods
	Character (double hp, double mp, string name) : maxHP(hp), HP(hp), maxMP(mp), MP(mp), name(name) { cout << "Character created "; writeStatusFull(); };
};

/// lazy to write index incrementing for. For each can't modify elements inside of vector. Find out why!
#define ForEach(x) for(size_t i=0; i<(x).size(); i++)

int main(int argc, char *argv[])
{
	cout << "Hello world, prepare for life and death match of good and evil!" <<endl << endl;

	// init phase
	/// load from file
	Character kirito(50, 45, "Kirito");
	Character lisbeth(40, 50, "Lisbeth");
	Character silica(35, 60, "Silica");
	/// load based on char param in file
	vector<Character> goods = {kirito, lisbeth, silica};

	Character zaza(60, 60, "Death Gun");
	Character poh(80, 40, "PoH");
	vector<Character> evils = {zaza, poh};

	/// load from file
	Attack weakAtt("Jab", 10, 4);
	Attack strongAtt("Hero Slash", 10, 7);

	// duel setup phase
	/// make list of attacks known by char connected with existing attacks, remove this
	ForEach(goods) { goods[i].setAttacks(vector<Attack>(1, strongAtt)); }
	ForEach(evils) { evils[i].setAttacks(vector<Attack>(1, weakAtt)); }

	/// make it choosable by player
	Character &champG = goods[1];
	Character &champE = evils[0];

	// duel phase
	int round = 1;
	champG.writeStatusFull();
	champE.writeStatusFull();
	// battle till both are standing or time runs out
	/// make time adjustable
	while (champG.isAlive() && champE.isAlive() && round < 100) {
		static double dmg = 0;
		cout <<endl <<"Round " <<round++ <<endl;
		champG.writeStatus();
		champE.writeStatus();

		// make moves/attacks
		/// consider attack speed to determine attack order if there is any difference in attack power based on health
		// player attack
		/// let player choose the attack
		dmg = champG.attack(0);
		if (dmg > 0.0) champE.dealDmg(dmg);
		else champG.restoreMP();

		// oponent attack
		///choose attack by random / AI
		dmg = champE.attack(0);
		if (dmg > 0.0) champG.dealDmg(dmg);
		else champE.restoreMP();
	}

	// end of fight
	cout <<endl <<"Winner is: " << (champE.isAlive() ? champE.getName() : champG.getName()) <<(round >= 100 ? " by time." : ".")<<endl;
	/// ask for another match and repeat

	return 0;
}


